# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
from sqlalchemy.orm import declarative_base
from sqlalchemy import Column, String, Integer, create_engine
from sqlalchemy_utils import UUIDType

Base = declarative_base()


class User(Base):
    __tablename__ = "User"

    SysRecId = Column(Integer, primary_key=True, autoincrement=True)
    Staff_Guid = Column(UUIDType)
    Staff_Name = Column(String)


def main():
    pass


if __name__ == '__main__':
    main()
