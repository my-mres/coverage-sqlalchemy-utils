from sqlalchemy.orm import Session
import random
from sqlalchemy import create_engine
from main import Base, User
import pytest
import string

@pytest.fixture
def in_mem_db() -> Session:
    sqlite_shared_name = "test_db_{}".format(random.sample(string.ascii_letters, k=4))
    engine = create_engine(
        "sqlite:///file:{}?mode=memory&cache=shared&uri=true".format(
            sqlite_shared_name
        ),
        echo=True,
    )
    session = Session(engine)
    Base.metadata.create_all(engine)
    return session


def test_user(in_mem_db):
    user = User()
    assert user is not None